#!/bin/bash
# Add any bash functions needed in the pipeline to this file.
set -euo pipefail

# Write a bold green message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in green
function echo_green {
    echo -e "\e[1;32m${1}\e[0m"
}

# Write a bold red message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in red
function echo_red {
    echo -e "\e[1;31m${1}\e[0m"
}

# Write a bold yellow message (that looks like gitlab-runner's messages).
# Args:
#  $1: Message to print in red
function echo_yellow {
    echo -e "\e[1;33m${1}\e[0m"
}

# Write a stylized header, but fall back to plain text
# Args:
#  $@: Message to print
function say {
    echo "$@" | toilet -f mono12 -w 300 | lolcat -f || echo "$@"
}

# Use this to execute an application and log the invocation by prepending
# this to an existing command.
function run_logged() {
    bash -x -c '"$@"' -- "$@"
}

# Generate -v key=value arguments for kpet from the trigger variables given
# in KPET_*VARIABLES in the kpet_variable_arguments array.
function kpet_generate_variable_arguments() {
    kpet_variable_arguments=()
    for kpet_variable in $KPET_VARIABLES $KPET_ADDITIONAL_VARIABLES; do
        if [ -v "$kpet_variable" ]; then
            kpet_variable_arguments+=("-v" "${kpet_variable}=${!kpet_variable}")
        fi
    done
}

# Set an rc parameter.
# Args: section name value
function rc_set() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2" "$3"
}

# Get an rc parameter.
# Args: section name
# Output: value
function rc_get() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2"
}

# Delete an rc parameter.
# Args: section name
function rc_del() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2"
}

# Set an rc state parameter.
# Args: name value
function rc_state_set() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1" "$2"
}

# Get an rc state parameter.
# Args: name
# Output: value
function rc_state_get() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1"
}

# Delete an rc state parameter.
# Args: name
function rc_state_del() {
    ${VENV_PY3} ${PIPELINE_DEFINITION_DIR}/scripts/rc_edit.py "$RC_FILE" ${FUNCNAME[0]} "$1"
}

# Functions for calculating build time durations
#
# Store time before build start
function store_time() {
    BUILD_TIME_START=$(date +"%s")
}

# Calculate build time and store into rc
# Args: rc_variable
function calculate_duration() {
    echo $(($(date +"%s") - BUILD_TIME_START))
}

# Join an array using a delimiter.
function join_by() {
    local IFS="$1"
    shift
    echo "$*"
}

function upt_release_testing_resources() {
  local ARG_PY3=$1
  local ARG_UPT_INPUT=$2

  "${ARG_PY3}" -m upt cancel -r "${ARG_UPT_INPUT}"
}

function upt_release_testing_resources_on_fatal_err() {
  local ARG_RETCODE=$1
  local ARG_PY3=$2
  local ARG_UPT_INPUT=$3
  local ARG_EXIT_CMD=$4

  if [ "${ARG_RETCODE}" -ne 0 ]; then
    upt_release_testing_resources "${ARG_PY3}" "${ARG_UPT_INPUT}"
    "${ARG_EXIT_CMD}" "${ARG_RETCODE}"
  fi
}

# Join an array with a multi-character delimiter, but ensure that
# delimiter is added to the first item in the array as well. This is
# helpful for adding '--with=' and '--without' to rpmbuild commands.
function join_by_multi() {
    local d=$1
    shift
    printf "%s" "${@/#/$d}"
}

# Get a modified git_url if authenticated access to a GitLab repo is needed.
# Original URL is returned if no auth is set up.
# Args:
#   $1: git_url variable
# Env variables:
#   $GITLAB_READ_REPO_TOKENS: json {"host": "token_name"}
#   Any variable specified by "token_name"
function get_auth_git_url {
    local original_url="$1"
    proto="${original_url%%://*}"          # Save protocol
    git_host="${original_url#*//}"         # Strip protocol
    git_host="${git_host%%/*}"             # Save host; strip everything after hostname
    git_repo_path="${original_url#*//*/}"  # Strip protocol and host

    token_name="$(jq --arg GIT_HOST "$git_host" -r '.[$GIT_HOST] // empty' <<< "${GITLAB_READ_REPO_TOKENS:-}")"
    if [ -v "${token_name}" ] ; then
        echo "${proto}://oauth2:${!token_name}@${git_host}/${git_repo_path}"
    else
        echo "$original_url"
    fi
}

function _git_cache_clone {
    aws_s3_download BUCKET_GIT_CACHE "${owner_repo}.tar" - | \
        tar -xf - -C "${GIT_CACHE_DIR}/${owner_repo}"
}

# Clone a repository from the git-cache.
# Args:
#   $1: complete repository URL
#   $@: additional arguments for git clone, e.g. working directory
function git_cache_clone {
    local owner_repo
    owner_repo="$(get_owner_repo "$1")"
    rm -rf "${GIT_CACHE_DIR:?}/${owner_repo}"
    mkdir -p "${GIT_CACHE_DIR}/${owner_repo}"
    loop _git_cache_clone
    shift
    git clone --quiet "${GIT_CACHE_DIR}/${owner_repo}" "$@"
    rm -rf "${GIT_CACHE_DIR:?}/${owner_repo}"
}

# Attempt to clone from the upstream source first. If that fails, fall back
# to our cached clones that are updated every 15 minutes.
# Args:
#   $1: repository URL
#   $2: directory for cloned repository
#   $@: additional arguments for git clone when cloning from upstream
function git_clone {
    local repo_url clone_dir
    repo_url=$(git_clean_url "$1")
    clone_dir=$2
    shift 2

    echo_green "Cloning ${repo_url} from upstream"
    if ! loop git clone --quiet "${repo_url}" "${clone_dir}" "$@"; then
        echo_yellow "Cloning from upstream failed, attempting to clone from cache"
        git_cache_clone "${repo_url}" "${clone_dir}"
    fi
}

# Run the reliable git cloner for repos on gitlab.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
#   $@: additional arguments for git clone
function git_clone_gitlab_com {
    local repo_name=$1
    local clone_dir=$2
    shift 2
    git_clone "gitlab.com/cki-project/${repo_name}.git" "${clone_dir}" "$@"
}

# Run the reliable git cloner for repos on gitlab.cee.redhat.com.
# Args:
#   $1: bare repo name (such as 'skt' or 'kpet')
#   $2: directory for cloned repository
#   $@: additional arguments for git clone
function git_clone_gitlab_cee {
    local repo_name=$1
    local clone_dir=$2
    shift 2
    git_clone "gitlab.cee.redhat.com/cki-project/${repo_name}.git" "${clone_dir}" "$@"
}

# Get the short tag for a commit in a repository.
# Args:
#   $1: path to the git repository
function get_short_tag {

    # Does the repo directory exist?
    if [[ ! -d $1 ]]; then
        exit 1
    fi

    # Switch to the repo directory.
    pushd $1 > /dev/null

    # Describe the most recent commit.
    TAG=$(git describe --abbrev=0 || true)

    if [[ $TAG =~ ^kernel ]]; then
        # Handle kernel tags in kernel-VERSION-RELEASE format.
        SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 3-)
    elif [[ $TAG =~ ^[0-9] ]]; then
        # Handle kernel tags in VERSION-RELEASE format.
        SHORT_TAG=$(echo ${TAG} | cut -d '-' -f 2-)
    else
        # Handle upstream kernel tags, such as v5.0-rc8, or situations where
        # there are no tags at all.
        SHORT_TAG=$(git rev-list --max-count=1 HEAD | cut -b-7)
    fi

    # Return the short tag we generated with a dash prepended.
    echo "-${SHORT_TAG}"

    popd > /dev/null
}

# Get the owner.repo name that is used in the git-cache for a repository.
# Args:
#   $1: url to the git repository
function get_owner_repo {
    GIT_URL="$1"
    GIT_URL_LC="${GIT_URL,,}"                 # everything lowercase
    GIT_URL_PATH="${GIT_URL_LC#*//*/}"        # strip protocol and host
    GIT_URL_PATH="${GIT_URL_PATH%/}"          # strip trailing slashes
    GIT_URL_PATH="${GIT_URL_PATH%.git}"       # strip optional .git suffix
    GIT_URL_REPO="${GIT_URL_PATH##*/}"        # strip any directories
    if [[ "${GIT_URL_PATH}" == */* ]]; then
        GIT_URL_OWNER="${GIT_URL_PATH%/*}"    # strip repo
        GIT_URL_OWNER="${GIT_URL_OWNER##*/}"  # strip any parent directories
    else
        # default to "git" owner
        # this makes the following URLs compatible (cgit):
        # - git://host.com/repo.git
        # - http://host.com/git/repo.git
        GIT_URL_OWNER="git"
    fi
    echo "${GIT_URL_OWNER}.${GIT_URL_REPO}"
}

# Determine the total number of CPUs available.
#
# NOTE(mhayden): This process must be done carefully since `nproc` in a
# container may say that 64 CPUs are available, but cgroups have limited the
# CPU count to 4. Tools like make, rpm, and xz are not able to determine
# cgroup limits, so they will use the CPU count from the system instead. This
# causes reduced performance and often causes OOM errors.
function get_cfs_quota {
    local QUOTA_FILE="/sys/fs/cgroup/cpu/cpu.cfs_quota_us"
    if [ -f "${QUOTA_FILE}" ]; then
        cat "${QUOTA_FILE}"
    fi
}

function get_cpu_count {
    local CFS_QUOTA CPUS_AVAILABLE MAKE_JOBS RPM_BUILD_NCPUS
    CFS_QUOTA="$(get_cfs_quota)"
    if [ -n "${CFS_QUOTA}" ] && [ "${CFS_QUOTA}" != "-1" ]; then
        CPUS_AVAILABLE="$((CFS_QUOTA / 100 / 1000))"
    else
        CPUS_AVAILABLE="$(nproc)"
    fi

    # On the off chance that the CPU count is less than 1, set the count to 1.
    if [ "${CPUS_AVAILABLE}" -lt 1 ]; then
        CPUS_AVAILABLE=1
    fi

    # Set the number of make jobs based on kernel developer recommendations.
    export MAKE_JOBS="$((CPUS_AVAILABLE * 3 / 2))"

    # RPM needs a special environment variable set so that this command returns
    # the correct number of CPUs in the container: rpm --eval %{_smp_mflags} If
    # this is not set, too many XZ_THREADS will be spawned and we will get OOM
    # errors. See FASTMOVING-1526.
    export RPM_BUILD_NCPUS="${MAKE_JOBS}"

    echo 'export CPUS_AVAILABLE="'"${CPUS_AVAILABLE}"'"'
    echo 'export MAKE_JOBS="'"${MAKE_JOBS}"'"'
    echo 'export RPM_BUILD_NCPUS="'"${RPM_BUILD_NCPUS}"'"'
}

eval "$(sed -n '/AWS-BEGIN/,/AWS-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"
eval "$(sed -n '/GENERAL-BEGIN/,/GENERAL-END/p' "${BASH_SOURCE[0]%/*}/../cki_pipeline.yml")"

function override {
    local TYPE="$1"
    local PACKAGE_NAME="$2"
    local PACKAGE_NAME_SANITIZED=${PACKAGE_NAME/-/_}
    local PIP_URL="${PACKAGE_NAME_SANITIZED}_pip_url"
    local RESOLVED_PIP_URL="${!PIP_URL:-}"

    if [[ -n "${RESOLVED_PIP_URL}" ]]; then
        echo_yellow "Found ${PACKAGE_NAME} override: ${RESOLVED_PIP_URL}"
        if [ "$TYPE" = "package" ]; then
            ${VENV_PY3} -m pip uninstall -y "${PACKAGE_NAME}"
            ${VENV_PY3} -m pip install "${RESOLVED_PIP_URL}"
        else
            local BRANCH=${RESOLVED_PIP_URL##*@}
            local REPO=${RESOLVED_PIP_URL%@*}
            REPO="$(git_clean_url "${REPO}")"
            rm -rf "${SOFTWARE_DIR:?}/${PACKAGE_NAME}"
            git_clone "${REPO}" "${SOFTWARE_DIR}/${PACKAGE_NAME}" --depth 1 --branch "${BRANCH}"
        fi
    fi
}

# Run skt --help to see if skt works.
function dryrun_by_skt {
    local ARG_PY3=$1
    local ARG_BKR=$2
    local ARG_BKR_INPUT=$3
    # Verify that skt works properly even when doing a dry run.
    "${ARG_PY3}" -m skt.executable --help | head -n1

    # Check the job
    "${ARG_BKR}" job-submit --dry-run "${ARG_BKR_INPUT}"
    echo "SKT: Dry run was successful. Check the artifacts for beaker.xml to verify that the XML contents are correct."
}

# Run upt legacy convert and rcclient --help to see if UPT and restraint client works.
function dryrun_by_upt {
    local ARG_PY3=$1
    local ARG_UPT_INPUT=$2
    local ARG_UPT_OUTPUT=$3

    # Verify that we can convert beaker.xml
    "${ARG_PY3}" -m upt legacy convert -i "${ARG_UPT_INPUT}" -r "${ARG_UPT_OUTPUT}"
    # Verify that we can run restraint_wrap module.
    "${ARG_PY3}" -m restraint_wrap --help | head -n1
    echo "UPT: Dry run was successful. Check the artifacts for c_req.yaml to verify that the contents are correct."
}

# Run kernel testing using skt.
function test_by_skt {
    local ARG_TIMEOUT_BIN=$1
    local ARG_TIMEOUT_LENGTH=$2
    local ARG_PY3=$3
    local ARG_RC=$4

    "${ARG_TIMEOUT_BIN}" -v -k 300 "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" -m skt.executable -vv --state --rc "${ARG_RC}" run --wait || true
}

# Run kernel testing using restraint runner.
function test_by_restraint_runner {
    local ARG_TIMEOUT_BIN=$1
    local ARG_TIMEOUT_LENGTH=$2
    local ARG_PY3=$3
    local ARG_RC=$4
    local ARG_UPT_INPUT=$5
    local ARG_UPT_OUTPUT_DIR=$6
    local ARG_EXIT_CMD=$7

    local RETCODE=0
    "${ARG_TIMEOUT_BIN}" -v -k 300 "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" -m restraint_wrap test --rc "${ARG_RC}" --reruns 1 -i "${ARG_UPT_INPUT}" -o "${ARG_UPT_OUTPUT_DIR}/run" || RETCODE=$?
    # Always cancel job regardless of RC
    upt_release_testing_resources "${ARG_PY3}" "${ARG_UPT_INPUT}"
    # Exit with caught retcode, if non-zero
    if [ "${RETCODE}" -ne 0 ]; then
        "${ARG_EXIT_CMD}" "${RETCODE}"
    fi
}

## Run provisioning using upt.
function provision_by_upt {
    local ARG_TIMEOUT_BIN=$1
    local ARG_TIMEOUT_LENGTH=$2
    local ARG_PY3=$3
    local ARG_RC=$4
    local ARG_UPT_INPUT=$5
    local ARG_UPT_OUTPUT_YAML=$6
    local ARG_EXIT_CMD=$7

    local RETCODE=0
    # Convert beaker.xml file to c_req.yaml and do legacy Beaker provisioning.
    UPT_ARGS=("--rc" "${ARG_RC}" "legacy" "provision" "-i" "${ARG_UPT_INPUT}" "-r" "${ARG_UPT_OUTPUT_YAML}")

    "${ARG_TIMEOUT_BIN}" -v -k 300 "${ARG_TIMEOUT_LENGTH}" "${ARG_PY3}" -m upt "${UPT_ARGS[@]}" || RETCODE=$?
    upt_release_testing_resources_on_fatal_err "${RETCODE}" "${ARG_PY3}" "${ARG_UPT_INPUT}" "${ARG_EXIT_CMD}"
}

# Get a list of successfully built selftest sets
# Args:
#   $1: path to the selftest result file, each line in the "set_name: exit_code" format
function get_successful_selftests {
    built_sets=()
    while read -r result ; do
        RETCODE=$(echo -e "${result##*:}" | tr -d '[:space:]')
        if [ "${RETCODE}" = "0" ] ; then
            built_sets+=("${result%%:*}")
        fi
    done < "$1"
    # Get rid of the extra space at the beginning
    echo TARGETS="${built_sets[*]}"
}
