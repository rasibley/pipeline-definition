#!/usr/bin/env python3
"""Determine the correct stream for the kernel version."""
import re
import sys


def parse_nvr(somestring):
    """Try to parse the nvr in the given string."""
    somestring = somestring.strip()
    pattern = re.compile('^(.+)-(.+)$')
    match = pattern.search(somestring)
    if match:
        return match.group(1), match.group(2)
    return None


def compare_lists(list1, list2):
    # pylint: disable=too-many-return-statements
    """Compare two version lists and return one of -1, 0, 1."""
    i = 0
    while i < len(list1) and i < len(list2):
        part1 = list1[i]
        part2 = list2[i]

        if part1.isdigit():
            if part2.isdigit():
                part1 = int(part1)
                part2 = int(part2)
            else:
                return 1
        else:
            if part2.isdigit():
                return -1

        if part1 < part2:
            return -1
        if part1 > part2:
            return 1
        i = i + 1

    if len(list1) > len(list2):
        return 1
    if len(list1) < len(list2):
        return -1
    return 0


def strip_el_part(rel):
    """Strip any el* suffix from an nvr."""
    if re.match('el[0-9]+(_[0-9]+){0,1}', str(rel[-1])):
        del rel[-1]

    return rel


def compare_nvr(nvr1str, nvr2str):
    """Compare two nvrs and return one of -1, 0, 1."""
    nvr1 = parse_nvr(nvr1str)
    nvr2 = parse_nvr(nvr2str)

    if nvr1 and nvr2:
        pass
    else:
        return None

    ver1, rel1 = nvr1
    ver2, rel2 = nvr2

    ver1 = ver1.split('.')
    ver2 = ver2.split('.')

    rel1 = rel1.split('.')
    rel2 = rel2.split('.')

    rel1 = strip_el_part(rel1)
    rel2 = strip_el_part(rel2)

    list1 = ver1 + rel1
    list2 = ver2 + rel2

    return compare_lists(list1, list2)


def nvr_in_range(nvr, nvr_low, nvr_high):
    """Check that the nvr is in the given range (inclusive)."""
    # pylint: disable=chained-comparison
    # chaining on the 0 makes no sense
    return (compare_nvr(nvr_low, nvr) <= 0 and
            compare_nvr(nvr_high, nvr) >= 0)


mapping = {
    # For some streams like rhel7 we distinguish streams (e.g. z-streams) with
    # specific requirements.
    'rhel5': {
        'rhel59-z': ('2.6.18-348.1.1.el5', '2.6.18-348.999.999.el5'),
        'rhel5-els': ('2.6.18-1.el5', '2.6.18-999.el5')
    },
    'rhel7': {
        'rhel72-z': ("3.10.0-327.0.1.el7",  "3.10.0-327.999.999.el7"),
        'rhel73-z': ("3.10.0-514.0.1.el7",  "3.10.0-514.999.999.el7"),
        'rhel74-z': ("3.10.0-693.0.1.el7",  "3.10.0-693.999.999.el7"),
        'rhel76-z': ("3.10.0-957.0.1.el7",  "3.10.0-957.999.999.el7"),
        'rhel77-z': ("3.10.0-1062.0.1.el7", "3.10.0-1062.999.999.el7"),
        'rhel78-z': ("3.10.0-1127.0.1.el7", "3.10.0-1127.999.999.el7"),
        'rhel79-z': ("3.10.0-1160.0.1.el7", "3.10.0-1160.999.999.el7"),
    },
    'rhel7-rt': {
        'rhel76-z-rt': ("3.10.0-957.0.1.el7",  "3.10.0-957.999.999.el7"),
        'rhel79-z-rt': ("3.10.0-1160.0.1.el7", "3.10.0-1160.999.999.el7"),
    },
    'rhel8': {
        'rhel80-z': ("4.18.0-80.0.1.el8_0",   "4.18.0-80.999.999.el8_0"),
        'rhel81-z': ("4.18.0-147.0.1.el8_1",  "4.18.0-147.999.999.el8_1"),
        'rhel82-z': ("4.18.0-193.0.1.el8_2",  "4.18.0-193.999.999.el8_2"),
        'rhel8':    ("4.18.0-0.el8",          "4.18.0-999.el8"),
    },
    'rhel8-rt': {
        'rhel80-z-rt': ("4.18.0-80.0.1.el8_0",  "4.18.0-80.999.999.el8_0"),
        'rhel81-z-rt': ("4.18.0-147.0.1.el8_1", "4.18.0-147.999.999.el8_1"),
        'rhel82-z-rt': ("4.18.0-193.0.1.el8_2", "4.18.0-193.999.999.el8_2"),
        'rhel8-rt':    ("4.18.0-0.el8",         "4.18.0-999.el8"),
    }
}


def main(args):
    """Run the main command line interface."""
    stream_type = args[0]
    nvr = args[1]

    # Strip kernel-/kernel-rt- prefix so the rest of the code is consistent
    if nvr.startswith('kernel-rt-'):
        nvr = nvr[len('kernel-rt-'):]
    elif nvr.startswith('kernel-'):
        nvr = nvr[len('kernel-'):]

    if stream_type in mapping.keys():
        # We have specified more precise mapping for rhels, so we'll use NVR
        # to map kernel to stream.
        nvr_found = False

        distinguished_stream = mapping[stream_type]
        for stream, nvr_range in distinguished_stream.items():
            nvr_low, nvr_high = nvr_range
            if nvr_in_range(nvr, nvr_low, nvr_high):
                print(stream)
                # mark: found
                nvr_found = True
                # take the first match, stop searching
                break
        if not nvr_found:
            # Raise exception here, so we can fixup stream mapping and restart
            # the pipeline.
            raise RuntimeError('stream for this nvr was not found')
    else:
        # pass-through
        print(stream_type)


if __name__ == '__main__':
    main(sys.argv[1:])
